import 'package:druginfomobileapp/service/service_data.dart';

import '/cubits/app_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppCubits extends Cubit<CubitStates> {
  AppCubits({
    required this.dataTrend,
    // required this.data,
  }) : super(
          InitialStates(),
        ) {
    emit(
      WellcomeState(),
    );
  }
  // final DataService data;
  // late final places;
  final TrendingData dataTrend;
  late final trending;
  // Future<void> getData() async {
  //   try {
  //     emit(
  //       LoadingState(),
  //     );
  //     places = await data.getInfo("ho");
  //     emit(
  //       LoadedState(places),
  //     );
  //   } catch (e) {}
  // }

  Future<void> getDataTrending() async {
    try {
      emit(
        LoadingState(),
      );
      trending = await dataTrend.getTrending();
      emit(LoadedState(trending));
    } catch (e) {}
  }
}
