import '/cubits/search_cache/searchcache_cubit.dart';
import '/views/navscreen/main_screen.dart';
import '../views/navscreen/search/pages/search_screen.dart';
import '/views/pages/wellcome_screen.dart';

import 'app_cubits.dart';
import '/cubits/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppCubitsLogic extends StatefulWidget {
  const AppCubitsLogic({Key? key}) : super(key: key);

  @override
  _AppCubitsLogicState createState() => _AppCubitsLogicState();
}

class _AppCubitsLogicState extends State<AppCubitsLogic> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AppCubits, CubitStates>(builder: (context, state) {
        if (state is WellcomeState) {
          return const WellcomeScreen();
        }
        if (state is LoadedState) {
          return const MainScreen();
        }
        if (state is LoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          if (state is SearchcacheInitial) {
            return const SearchScreen();
          } else {
            return Container();
          }
        }
      }),
    );
  }
}