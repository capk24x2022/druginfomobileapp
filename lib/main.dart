import 'package:druginfomobileapp/cubits/app_cubits.dart';
import 'package:druginfomobileapp/cubits/app_cubits_logic.dart';
import 'package:druginfomobileapp/models/data_model.dart';
import 'package:druginfomobileapp/routes/app_routes.dart';
import 'package:druginfomobileapp/service/service_data.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:firebase_core/firebase_core.dart' as firebase_core;

void main() async {
  Hive.registerAdapter<DataModel>(DataModelAdapter());
  await Hive.initFlutter();
  await Hive.openBox("search-cache");
  WidgetsFlutterBinding.ensureInitialized();
  await firebase_core.Firebase.initializeApp(
    name: 'DrugInfo',
    options: FirebaseOptions(
      apiKey: '...',
      appId: '...',
      messagingSenderId: '...',
      projectId: '...',
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'Home Page';

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      home: const MainPage(),
      onGenerateRoute: AppRoutes.onGeneratedRoutes,
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: BlocProvider<AppCubits>(
          create: (context) => AppCubits(
            dataTrend: TrendingData(),
            // data: DataService(),
          ),
          child: AppCubitsLogic(),
        ),
      );
}
