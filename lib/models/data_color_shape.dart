final List<String> colors = [
  'red',
  'orange',
  'yellow',
  'green',
  'blue',
  'pink',
  'purple'
];

final List<String> shapes = [
  'round',
  'triangle',
  'rectangle',
  'oval',
  'bullet',
  'diamond'
];
