import 'package:hive/hive.dart';

part 'data_model.g.dart';

@HiveType(typeId: 1)
class DataModel {
  @HiveField(0)
  String title;
  @HiveField(1)
  String locationType;
  @HiveField(2)
  int woeid;
  @HiveField(3)
  String lattLong;
  DataModel({
    required this.title,
    required this.locationType,
    required this.lattLong,
    required this.woeid,
  });

  factory DataModel.fromJon(Map<String, dynamic> jsonData) {
    return DataModel(
      title: jsonData['title'],
      locationType: jsonData['location_type'],
      lattLong: jsonData['latt_long'],
      woeid: jsonData['woeid'],
    );
  }
}


