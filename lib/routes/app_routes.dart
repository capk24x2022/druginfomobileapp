import 'package:druginfomobileapp/cubits/search_cache/searchcache_cubit.dart';

import 'package:druginfomobileapp/utils/screen_arguments.dart';
import 'package:druginfomobileapp/views/detail_screen/drug_details.dart';
import 'package:druginfomobileapp/views/detail_screen/post_details.dart';
import 'package:druginfomobileapp/views/navscreen/search/widgets/search_nav.dart';
import 'package:druginfomobileapp/views/pages/wellcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRoutes {
  static MaterialPageRoute getMaterialRoute(screen) {
    return MaterialPageRoute(builder: (context) => screen);
  }

  static PageRouteBuilder getPageRoute(screen) {
    return PageRouteBuilder(
      pageBuilder: (context, _, __) => screen,
      transitionDuration: Duration(
        milliseconds: 300,
      ),
      reverseTransitionDuration: Duration(
        milliseconds: 300,
      ),
    );
  }

  static Route? onGeneratedRoutes(RouteSettings route) {
    switch (route.name) {
      case "/":
        return getPageRoute(WellcomeScreen());

      case "/search":
        final SearchArgs? args = route.arguments as SearchArgs?;
        return getMaterialRoute(
          BlocProvider(
            create: (context) => SearchcacheCubit(),
            child: SearchNav(
              searchKeyword: args == null ? null : args.searchKeyword,
            ),
          ),
        );

      case "/postDetails":
        return getMaterialRoute(PostDetails());

      case "/drugsDetails":
        return getMaterialRoute(DrugDetails());
    }
  }
}
