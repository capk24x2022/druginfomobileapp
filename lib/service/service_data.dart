import 'package:druginfomobileapp/models/data_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DataService {
  String baseUrl = "https://www.metaweather.com/api";
  var locationUrl = (search) => "/location/search/?query=${search}";

  Future<List<DataModel>> getInfo(String search) async {
    http.Response res =
        await http.get(Uri.parse(baseUrl + locationUrl(search)));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        return list.map((e) => DataModel.fromJon(e)).toList();
      } else {
        return <DataModel>[];
      }
    } catch (e) {
      print(e);
      return <DataModel>[];
    }
  }
}

// get a list of data item in api
class TrendingData {
  String url = "https://www.metaweather.com/api/location/search/?query=san";
  Future<List<DataModel>> getTrending() async {
    http.Response resTrend = await http.get(Uri.parse(url));
    if (resTrend.statusCode == 200) {
      List<dynamic> listTrend = json.decode(resTrend.body);
      return listTrend.map((e) => DataModel.fromJon(e)).toList();
    } else {
      return <DataModel>[];
    }
  }
}

// typeahead search call api
class typeAhead {
  static Future<List<Map<String, String>>> getTypeAhead(String query) async {
    if (query.isEmpty && query.length < 3) {
      print("Query needs to be atleast 3 chars");
      return Future.value([]);
    }
    var searchUrl =
        "https://www.metaweather.com/api/location/search/?query=${query}";
    http.Response resTypeAhead = await http.get(Uri.parse(searchUrl));
    List<DataModel> suggestion = [];
    if (resTypeAhead.statusCode == 200) {
      Iterable listTrend = json.decode(resTypeAhead.body);
      suggestion =
          List<DataModel>.from(listTrend.map((e) => DataModel.fromJon(e)));
    } else {
      print('Request failed with status: ${resTypeAhead.statusCode}.');
    }
    return Future.value(suggestion.map((e) => {'title': e.title}).toList());
  }
}
