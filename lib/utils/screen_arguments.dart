class SearchArgs {
  final String searchKeyword;

  SearchArgs({
    required this.searchKeyword,
  });
}
