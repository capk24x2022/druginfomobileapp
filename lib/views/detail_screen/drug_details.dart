import 'package:druginfomobileapp/theme/palette.dart';
import 'package:druginfomobileapp/widgets/app_text.dart';
import 'package:druginfomobileapp/widgets/header.dart';
import 'package:flutter/material.dart';

class DrugDetails extends StatefulWidget {
  DrugDetails({Key? key}) : super(key: key);

  @override
  _DrugDetailsState createState() => _DrugDetailsState();
}

class _DrugDetailsState extends State<DrugDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, titleText: ""),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(right: 0, top: 0),
              width: MediaQuery.of(context).size.width,
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/drugs_pill/002282029.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppText(
                  text: "Trulicity Pen",
                  color: Palette.textNo,
                  size: 40,
                  fontWeight: FontWeight.w500,
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.volume_up),
                  iconSize: 35,
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            AppText(
                text: "Generic name: ",
                color: Palette.textNo,
                size: 20,
                fontWeight: FontWeight.w400),
            SizedBox(
              height: 5,
            ),
            AppText(
                text: "Brand name: ",
                color: Palette.textNo,
                size: 20,
                fontWeight: FontWeight.w400),
            SizedBox(
              height: 5,
            ),
            AppText(
                text: "Dug class: ",
                color: Palette.textNo,
                size: 20,
                fontWeight: FontWeight.w400),
            SizedBox(
              height: 5,
            ),
            AppText(
                text: "Brand name: ",
                color: Palette.textNo,
                size: 20,
                fontWeight: FontWeight.w400),
          ],
        ),
      ),
    );
  }
}
