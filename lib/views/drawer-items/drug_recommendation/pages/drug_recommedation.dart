import 'package:flutter/material.dart';
import '/widgets/header.dart';

class DrugRecommendation extends StatelessWidget {
  const DrugRecommendation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: header(context, titleText: 'Gợi ý thuốc'),
    body: Center(
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    "assets/images/maintenance.png",
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Text(
              "Chức năng đang được phát triển.",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(height: 100)
        ],
      ),
    ),
  );
}
