import 'package:druginfomobileapp/widgets/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '/theme/palette.dart';
import '/views/drawer-items/signup/pages/signup.dart';
import '/views/navscreen/social_com.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  // form key
  final _formKey = GlobalKey<FormState>();

  // editing controller
  final TextEditingController emailController = new TextEditingController();

  // firebase
  final _auth = FirebaseAuth.instance;

  // string for displaying the error Message
  String? errorMessage;

  @override
  Widget build(BuildContext context) {
    //email field
    final emailField = TextFormField(
        autofocus: false,
        controller: emailController,
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value!.isEmpty) {
            return ("Hãy nhập email của bạn");
          }
          // reg expression for email validation
          if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
              .hasMatch(value)) {
            return ("Hãy nhập email hợp lệ");
          }
          return null;
        },
        onSaved: (value) {
          emailController.text = value!;
        },
        textInputAction: TextInputAction.next,
        style: TextStyle(color: Palette.p1),
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.mail,
            color: Colors.white,
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Mời bạn nhập tài khoản",
          hintStyle: TextStyle(color: Palette.p1),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    final loginButton = Material(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      color: Palette.p1,
      child: MaterialButton(
          padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            ResetAccount(emailController.text);
          },
          child: Text(
            "Gửi yêu cầu",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF527DAA),
                fontWeight: FontWeight.bold),
          )),
    );

    return Scaffold(
      backgroundColor: Palette.p6,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Palette.p6,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Text(
                        'Khôi phục tài khoản',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                        height: 100,
                        child: Image.asset(
                          "assets/icons/user_login.png",
                          fit: BoxFit.contain,
                        )),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Tài khoản',
                          style: kLabelStyle,
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    emailField,
                    SizedBox(height: 20),
                    loginButton,
                    SizedBox(height: 15),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Quay lại",
                              style: TextStyle(
                                  color: Palette.p1,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          )
                        ]
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // login function
  void ResetAccount(String email) async {
    if (_formKey.currentState!.validate()) {
      try {
        await _auth
            .sendPasswordResetEmail(email: email)
            .then((uid) => {
                  Navigator.of(context).pop(),
                });
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage =
                "Địa chỉ email của bạn dường như không đúng định dạng.";
            break;
          case "wrong-password":
            errorMessage = "Mật khẩu không hợp lệ";
            break;
          case "user-not-found":
            errorMessage = "Email này đã tồn tại";
            break;
          case "user-disabled":
            errorMessage = "Email này đã bị khóa";
            break;
          case "too-many-requests":
            errorMessage = "Quá nhiều yêu cầu";
            break;
          case "operation-not-allowed":
            errorMessage = "Đăng nhập bằng Email và Mật khẩu không được bật.";
            break;
          default:
            errorMessage = "Đã xảy ra lỗi không xác định.";
        }
        Fluttertoast.showToast(msg: errorMessage!);
        print(error.code);
      }
    }
  }
}
