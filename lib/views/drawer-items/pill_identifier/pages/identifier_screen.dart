import 'package:druginfomobileapp/models/data_color_shape.dart';
import 'package:druginfomobileapp/theme/palette.dart';
import 'package:druginfomobileapp/widgets/app_text.dart';
import 'package:druginfomobileapp/widgets/header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Identifier extends StatefulWidget {
  const Identifier({Key? key}) : super(key: key);

  @override
  _IdentifierState createState() => _IdentifierState();
}

class _IdentifierState extends State<Identifier> {
  List<String> itemsColors = [
    'Chọn một màu',
    'vàng',
    'đỏ',
    'xanh',
    'xám',
  ];

  List<String> itemsShapes = [
    'Chọn một hình dáng',
    'tròn',
    'con nhộng',
    'xanh',
    'xám',
  ];

  String dropdownColor = 'Chọn một màu';
  String dropdownShape = 'Chọn một hình dáng';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context, titleText: 'Tìm kiếm nâng cao'),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: AppText(
              text:
              "Sử dụng tìm kím nâng cao để xác định các loại thuốc bằng hình dáng bên ngoài hoặc tên thuốc.",
              color: Palette.textNo,
              size: 20,
              fontWeight: FontWeight.normal,
            ),
          ),
          Container(
            // height: 200,
            width: 350,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Palette.blueTheme,
                width: 2,
              ),
            ),
            padding: EdgeInsets.all(10),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppText(
                  text: "Chọn màu thuốc",
                  color: Palette.textNo,
                  size: 20,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField(
                  hint: Text("Màu thuốc"),
                  onChanged: (String? newColor) {
                    setState(() {
                      dropdownColor = newColor!;
                    });
                  },
                  items: itemsColors.map((e) {
                    return DropdownMenuItem(
                      child: Text(e),
                      value: e,
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 35,
                ),
                AppText(
                  text: "Chọn hình dạng thuốc",
                  color: Palette.textNo,
                  size: 20,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField(
                  hint: Text("Hình dạng thuốc"),
                  onChanged: (String? newShape) {
                    setState(() {
                      dropdownShape = newShape!;
                    });
                  },
                  items: itemsShapes.map((e) {
                    return DropdownMenuItem(
                      child: Text(e),
                      value: e,
                    );
                  }).toList(),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: 350,
                  height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Palette.blueTheme),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/drugsDetails");
                    },
                    child: AppText(
                      text: "Tìm kiếm",
                      color: Palette.whiteText,
                      size: 20,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
