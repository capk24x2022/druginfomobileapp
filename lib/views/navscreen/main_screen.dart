import 'search/pages/search_screen.dart';
import '/views/navscreen/social_com.dart';
import '/widgets/navigation_drawer_widget.dart';
import '/theme/palette.dart';
import '/views/pages/home_screen.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List items = [
    const HomeScreen(),
    const SearchScreen(),
    const SocialCom(),
  ];

  int currentIndex = 0;

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawerWidget(),
      // endDrawer: NavigationDrawerWidget(),
      body: items[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: onTap,
        elevation: 0,
        unselectedFontSize: 0,
        selectedFontSize: 0,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Palette.p6,
        selectedItemColor: Palette.p1,
        unselectedItemColor: Palette.p1.withOpacity(0.5),
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.apps,
              size: 35,
            ),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search_outlined,
              size: 35,
            ),
            title: Text('Search'),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.groups,
              size: 35,
            ),
            title: Text('Social'),
          ),
        ],
      ),
    );
  }
}