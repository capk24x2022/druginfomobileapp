import 'package:druginfomobileapp/widgets/typeahead_search_bar.dart';

import '/cubits/app_cubits.dart';
import '/cubits/app_state.dart';
import '/widgets/header.dart';
import '/widgets/navigation_drawer_widget.dart';
import 'package:druginfomobileapp/theme/palette.dart';
import 'package:druginfomobileapp/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({
    Key? key,
  }) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  int selectedIndex = -1;
  int selectedIndexDrugs = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawerWidget(),
      appBar: header(context, titleText: 'Tìm kiếm thuốc'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10),
            //Thanh tìm kiếm
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: TypeAheadSearchBar(),
            ),
            SizedBox(
              height: 5,
            ),
            Divider(),
            SizedBox(
              height: 5,
            ),
            AppText(
              text: "Tìm kiếm nhiều nhất",
              color: Palette.textNo,
              size: 20,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(
              height: 5,
            ),
            BlocBuilder<AppCubits, CubitStates>(
              builder: (context, state) {
                if (state is LoadedState) {
                  var infor = state.places;
                  return Container(
                    height: 300,
                    width: 250,
                    child: ListView.builder(
                      itemCount: infor.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: EdgeInsets.only(left: 10),
                          child: GestureDetector(
                            child: AppText(
                              text: infor[index].title,
                              color: Palette.textNo,
                              size: 20,
                              fontWeight: FontWeight.normal,
                            ),
                            onTap: () {
                              Navigator.pushNamed(context, "/drugsDetails");
                            },
                          ),
                        );
                      },
                    ),
                  );
                } else {
                  return Container();
                }
              },
            )
          ],
        ),
      ),
    );
  }
}



// TextField(
//                 onTap: () {
//                   Navigator.pushNamed(context, "/search");
//                 },
//                 // enabled: false,
//                 showCursor: false,
//                 readOnly: true,
//                 decoration: InputDecoration(
//                   hintText: "Nhấn để nhập tên thuốc bất kì...",
//                   border: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(10),
//                     borderSide: BorderSide(
//                       color: Palette.p1.withOpacity(0.5),
//                     ),
//                   ),
//                 ),
//               ),