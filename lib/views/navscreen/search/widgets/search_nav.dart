import 'package:druginfomobileapp/cubits/app_cubits.dart';
import 'package:druginfomobileapp/cubits/app_state.dart';
import 'package:druginfomobileapp/views/navscreen/search/widgets/search_history_list.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '/cubits/search_cache/searchcache_cubit.dart';
import 'package:druginfomobileapp/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:druginfomobileapp/theme/palette.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchNav extends StatefulWidget {
  final String? searchKeyword;
  const SearchNav({
    Key? key,
    required this.searchKeyword,
  }) : super(key: key);

  @override
  _SearchNavState createState() => _SearchNavState();
}

class _SearchNavState extends State<SearchNav> {
  final TextEditingController _textEditingController = TextEditingController();

  Future<void> _updateSearchCache(
      String searchKeyword, SearchcacheCubit cubit) async {
    await cubit.updateSearchHistory(searchKeyword);
  }

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<SearchcacheCubit>(context);
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: header(context, titleText: "Tìm Kiếm"),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: size.width,
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Container(
                width: 350,
                child: TextField(
                  onSubmitted: (searchKeyword) {
                    if (searchKeyword.length > 0) {
                      _updateSearchCache(searchKeyword, cubit);
                    }
                  },
                  controller: _textEditingController,
                  decoration: InputDecoration(
                    hintText: "Nhập tên thuốc bất kì...",
                    suffixIcon: IconButton(
                      onPressed: () {
                        _textEditingController.clear();
                      },
                      icon: Icon(Icons.clear),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Palette.p1.withOpacity(0.5),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Divider(),
              BlocBuilder<SearchcacheCubit, SearchcacheState>(
                  builder: (context, state) {
                if (state is SearchcacheLoaded) {
                  return Expanded(
                    child: SearchHistoryList(
                        searchHistory: cubit.searchHistory!,
                        size: size,
                        callback: (String searchText) {
                          setState(() {
                            _textEditingController.text = searchText;
                          });
                          _updateSearchCache(searchText, cubit);
                        }),
                  );
                } else {
                  return Container();
                }
              })
            ],
          ),
        ),
      ),
    );
  }
}
