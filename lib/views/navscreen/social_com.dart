import 'package:flutter/material.dart';
import '/widgets/navigation_drawer_widget.dart';
import '/widgets/header.dart';

class SocialCom extends StatefulWidget {
  const SocialCom({Key? key}) : super(key: key);

  @override
  _SocialComState createState() => _SocialComState();
}

class _SocialComState extends State<SocialCom> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawerWidget(),
      appBar: header(context, titleText: 'Mạng xã hội'),
      body: Center(
        child: Column(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      "assets/images/maintenance.png",
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Text(
                "Chức năng đang được phát triển.",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            SizedBox(height: 100)
          ],
        ),
      ),
    );
  }
}