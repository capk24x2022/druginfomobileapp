import '/cubits/app_cubits.dart';
import '/cubits/app_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '/widgets/header.dart';
import '/widgets/navigation_drawer_widget.dart';

import '/theme/palette.dart';
import '/widgets/app_text.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  List imagesTren = [
    "002282029.jpg",
    "005550972.jpg",
    "005550973.jpg",
    "493480042.jpg",
    "521520215.jpg",
    "634810629.jpg",
    "646790936_PB.jpg",
    "646790937_PB.jpg",
  ];
  List imagesFav = [
    "Alprazolam 1 mg-ACT.jpg",
    "asacol-hd.jpg",
    "buspirone-hydrochloride.jpg",
    "Calcet.jpg",
    "clonazepam.jpg",
    "depakote.jpg",
    "isotretinoin.jpeg",
    "midodrine-hydrochloride.jpg",
    "16571-0402-50_NLMIMAGE10_903AC856.jpg",
  ];
  var imagesIcon = {
    "compare-drugs.png": "So sánh",
    "drugs-az.png": "Tìm kiếm",
    "fda-alerts.png": "FDAcảnh báo",
    "interactions-checker.png": "Tương kị",
    "pill-identifier.png": "Nhận dạng",
    "side-effects.png": "Phản ứng phụ",
  };
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(
      length: 2,
      vsync: this,
    );
    return Scaffold(
      drawer: const NavigationDrawerWidget(),
      appBar: header(context, titleText: 'Trang chủ'),
      body: BlocBuilder<AppCubits, CubitStates>(
        builder: (context, state) {
          if (state is LoadedState) {
            var info = state.places;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Container(
                //   padding: EdgeInsets.only(top: 40, left: 20),
                //   child: Row(
                //     children: [
                //       Icon(
                //         Icons.menu,
                //         size: 40,
                //         color: Palette.p5,
                //       ),
                //       Expanded(child: Container()),
                //       Container(
                //         margin: EdgeInsets.only(right: 20),
                //         width: 50,
                //         height: 50,
                //         decoration: BoxDecoration(
                //             borderRadius: BorderRadius.circular(10),
                //             color: Palette.p5),
                //       )
                //     ],
                //   ),
                // ),
                SizedBox(
                  height: 10,
                ),
                // Container(
                //   margin: EdgeInsets.only(left: 20),
                //   child: AppText(
                //     text: info[0].name,
                //     color: Palette.textNo,
                //     size: 20,
                //     fontWeight: FontWeight.w500,
                //   ),
                // ),
                SizedBox(
                  height: 0,
                ),
                Container(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: TabBar(
                      labelColor: Palette.textNo,
                      labelPadding: EdgeInsets.only(left: 20, right: 20),
                      unselectedLabelColor: Palette.textNo.withOpacity(0.5),
                      indicatorSize: (TabBarIndicatorSize.label),
                      indicator: Cricle(color: Palette.blueTheme, radius: 4),
                      isScrollable: true,
                      controller: _tabController,
                      tabs: [
                        Tab(
                          child: AppText(
                              text: "Xu Hướng",
                              color: Palette.p7,
                              size: 18,
                              fontWeight: FontWeight.normal),
                        ),
                        Tab(
                          child: AppText(
                              text: "Yêu Thích",
                              color: Palette.redTheme,
                              size: 18,
                              fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20),
                  height: 250,
                  width: double.maxFinite,
                  child: TabBarView(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, "/postDetails");
                        },
                        child: ListView.builder(
                          itemCount: imagesTren.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin: EdgeInsets.only(right: 20, top: 10),
                              width: 300,
                              height: 400,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/images/drugs_pill/" +
                                          imagesTren[index]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      ListView.builder(
                        itemCount: imagesFav.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: EdgeInsets.only(right: 20, top: 10),
                            width: 300,
                            height: 400,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              image: DecorationImage(
                                image: AssetImage("assets/images/drugs_pill/" +
                                    imagesFav[index]),
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                    controller: _tabController,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AppText(
                        text: "Khám phá thêm",
                        color: Palette.textNo,
                        size: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: double.maxFinite,
                  height: 100,
                  margin: EdgeInsets.only(
                    left: 20,
                  ),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: imagesIcon.keys.length,
                      itemBuilder: (_, index) {
                        return Container(
                          margin: EdgeInsets.only(right: 30),
                          child: Column(
                            children: [
                              Container(
                                width: 60,
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  image: DecorationImage(
                                    image: AssetImage(
                                      "assets/images/" +
                                          imagesIcon.keys.elementAt(index),
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                child: AppText(
                                  text: imagesIcon.values.elementAt(index),
                                  fontWeight: FontWeight.w400,
                                  color: Palette.textNo,
                                  size: 15,
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                ),
              ],
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

//Vẽ hình tròn
class Cricle extends Decoration {
  final Color color;
  double radius;

  Cricle({
    required this.color,
    required this.radius,
  });
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return CriclePainter(
      color: color,
      radius: radius,
    );
  }
}

class CriclePainter extends BoxPainter {
  final Color color;
  double radius;

  CriclePainter({
    required this.color,
    required this.radius,
  });
  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    Paint _paint = Paint();
    _paint.color = color;
    _paint.isAntiAlias = true;
    final Offset circleOffet = Offset(
        configuration.size!.width / 2 - radius / 2,
        configuration.size!.height - radius);
    canvas.drawCircle(offset + circleOffet, radius, _paint);
  }
}
