import 'package:druginfomobileapp/cubits/app_cubits.dart';
import 'package:druginfomobileapp/theme/palette.dart';
import 'package:druginfomobileapp/widgets/responsive_btn.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/widgets/app_text.dart';
import 'package:flutter/material.dart';

class WellcomeScreen extends StatefulWidget {
  const WellcomeScreen({Key? key}) : super(key: key);

  @override
  _WellcomeScreenState createState() => _WellcomeScreenState();
}

class _WellcomeScreenState extends State<WellcomeScreen> {
  List images = [
    "drugInfor_earch.png",
    "welcome-two.png",
    "welcome-three.png",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: images.length,
          itemBuilder: (_, index) {
            return Container(
              width: double.maxFinite,
              height: double.maxFinite,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/" + images[index]),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                margin: EdgeInsets.only(top: 150, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
/*                        Container(
                          width: 250,
                          child: AppText(
                            text: "Chào mừng bạn đến với Drugs Infor",
                            color: Palette.textNo,
                            size: 25,
                            fontWeight: FontWeight.w400,
                          ),
                        ),*/
                        SizedBox(
                          height: 10,
                        ),
/*                        Container(
                          margin: EdgeInsets.only(top: 20),
                          width: 250,
                          child: AppText(
                            text:
                                "Drugs Infor cung cấp cho người dùng những thông tin mới nhất về các loại thuốc, giúp người dùng tìm hiểu thêm về thông tin thuốc mà họ đang dùng hoặc sắp dùng",
                            color: Palette.textNo.withOpacity(0.5),
                            fontWeight: FontWeight.w400,
                            size: 20,
                          ),
                        ),*/
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            BlocProvider.of<AppCubits>(context).getDataTrending();
                          },
                          child: Container(
                            width: 200,
                            child: Row(
                              children: [
                                ResponsiveButton(
                                  color: Palette.blueTheme,
                                  width: 100,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),

                    //dots
                    Column(
                      children: List.generate(3, (indexDots) {
                        return Container(
                          width: 8,
                          margin: EdgeInsets.only(bottom: 5),
                          height: index == indexDots ? 8 : 8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: index == indexDots
                                ? Palette.p5
                                : Palette.p5.withOpacity(0.5),
                          ),
                        );
                      }),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
