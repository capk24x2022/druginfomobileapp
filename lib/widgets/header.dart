import 'package:flutter/material.dart';
import 'package:druginfomobileapp/theme/palette.dart';

AppBar header(context, {bool isAppTitle = false, required String titleText}) {
  return AppBar(
    title: Text(
      isAppTitle ? "FlutterShare" : titleText,
      style: TextStyle(
        color: Palette.whiteText,
        fontFamily: isAppTitle ? "Cairo-Bold" : "",
        fontSize: isAppTitle ? 50.0 : 22.0,
      ),
    ),
    centerTitle: true,
    backgroundColor: const Color(0xFF0D47A1),
  );
}
