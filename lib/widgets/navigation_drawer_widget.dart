import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:druginfomobileapp/models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '/views/drawer-items/pill_identifier/pages/identifier_screen.dart';
import '/views/drawer-items/capture_images/pages/image_capture_page.dart';
import '/views/drawer-items/compare_drugs/pages/compare_drug_screen.dart';
import '/views/drawer-items/health_profile/pages/health_profile.dart';
import '/views/drawer-items/bmi_calculator/pages/bmi.dart';
import '/views/drawer-items/drug_recommendation/pages/drug_recommedation.dart';
import '/views/drawer-items/health_checker/pages/health_checker.dart';
import '/views/drawer-items/signin/pages/signin.dart';
import '../views/drawer-items/profile/pages/profile.dart';
import '/views/drawer-items/signup/pages/signup.dart';
import '/theme/palette.dart';

class NavigationDrawerWidget extends StatefulWidget {
  const NavigationDrawerWidget({Key? key}) : super(key: key);

  @override
  _NavigationDrawerWidgetState createState() => _NavigationDrawerWidgetState();
}

class _NavigationDrawerWidgetState extends State<NavigationDrawerWidget> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  /*@override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());
      setState(() {});
    });
  }*/

  final padding = const EdgeInsets.symmetric(horizontal: 20);

  @override
  Widget build(BuildContext context) {

    Widget _CheckAuthentication;
    if (user != null) {
      _CheckAuthentication = new Container(
        width: 300,
        child: Drawer(
          child: Material(
            color: Palette.p6,
            child: ListView(
              children: <Widget>[
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Image.asset(
                            "assets/icons/user_login.png",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                          padding: padding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("${loggedInUser.firstName} ${loggedInUser.secondName}",
                                  style: TextStyle(
                                    color: Palette.p1,
                                    fontWeight: FontWeight.w500,
                                  )
                              ),
                              Text("${loggedInUser.email}",
                                  style: TextStyle(
                                    color: Palette.p1,
                                    fontWeight: FontWeight.w500,
                                  )
                              ),
                            ],
                          )
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  padding: padding,
                  child: Column(
                    children: [
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Tìm kiếm nâng cao',
                        icon: Icons.zoom_in,
                        onClicked: () => selectedItem(context, 1),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Báo cáo thuốc đến Admin',
                        icon: Icons.photo_camera_outlined,
                        onClicked: () => selectedItem(context, 2),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Tính chỉ số BMI',
                        icon: Icons.calculate_outlined,
                        onClicked: () => selectedItem(context, 3),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Kiểm tra tương kị thuốc',
                        icon: Icons.do_disturb_alt_outlined,
                        onClicked: () => selectedItem(context, 4),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'So sánh thuốc',
                        icon: Icons.compare_outlined,
                        onClicked: () => selectedItem(context, 5),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Gợi ý thuốc',
                        icon: Icons.recommend_outlined,
                        onClicked: () => selectedItem(context, 6),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Kiểm tra sức khỏe',
                        icon: Icons.health_and_safety_outlined,
                        onClicked: () => selectedItem(context, 7),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Hồ sơ sức khỏe',
                        icon: Icons.list_alt_outlined,
                        onClicked: () => selectedItem(context, 8),
                      ),
                      const SizedBox(height: 0),
                      Divider(color: Colors.white70),
                      buildMenuItem(
                        text: 'Đăng xuất',
                        icon: Icons.logout_outlined,
                        onClicked: () {
                          logout(context);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      _CheckAuthentication = new Container(
        width: 300,
        child: Drawer(
          child: Material(
            color: Palette.p6,
            child: ListView(
              children: <Widget>[
                Container(
                  padding: padding,
                  child: Column(
                    children: [
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Tìm kiếm nâng cao',
                        icon: Icons.zoom_in,
                        onClicked: () => selectedItem(context, 1),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Báo cáo thuốc đến Admin',
                        icon: Icons.photo_camera_outlined,
                        onClicked: () => selectedItem(context, 2),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Tính chỉ số BMI',
                        icon: Icons.calculate_outlined,
                        onClicked: () => selectedItem(context, 3),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Kiểm tra tương kị thuốc',
                        icon: Icons.do_disturb_alt_outlined,
                        onClicked: () => selectedItem(context, 4),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'So sánh thuốc',
                        icon: Icons.compare_outlined,
                        onClicked: () => selectedItem(context, 5),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Gợi ý thuốc',
                        icon: Icons.recommend_outlined,
                        onClicked: () => selectedItem(context, 6),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Kiểm tra sức khỏe',
                        icon: Icons.health_and_safety_outlined,
                        onClicked: () => selectedItem(context, 7),
                      ),
                      const SizedBox(height: 0),
                      buildMenuItem(
                        text: 'Hồ sơ sức khỏe',
                        icon: Icons.list_alt_outlined,
                        onClicked: () => selectedItem(context, 8),
                      ),
                      const SizedBox(height: 0),
                      Divider(color: Colors.white70),
                      buildMenuItem(
                        text: 'Đăng nhập',
                        icon: Icons.login_outlined,
                        onClicked: () => selectedItem(context, 9),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Container(
      child: _CheckAuthentication,
    );
  }

  Widget buildHeader({
    required String urlImage,
    required String name,
    required String email,
    required VoidCallback onClicked,
  }) =>
      InkWell(
        onTap: onClicked,
        child: Container(
          padding: padding.add(const EdgeInsets.symmetric(vertical: 40)),
          child: Row(
            children: [
              CircleAvatar(radius: 30, backgroundImage: NetworkImage(urlImage)),
              const SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: const TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    email,
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Fluttertoast.showToast(msg: "Đã thoát tài khoản");
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => SignInScreen()));
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    const color = Colors.white;
    const hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: const TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    final auth = FirebaseAuth.instance;
    Navigator.of(context).pop();
    switch (index) {
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const Identifier(),
        ));
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CaptureimagePage(),
        ));
        break;
      case 3:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CalculateBMI(),
        ));
        break;
      case 5:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CompareDrug(),
        ));
        break;
      case 6:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => DrugRecommendation(),
        ));
        break;
      case 7:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HealthChecker(),
        ));
        break;
      case 8:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HealthProfile(),
        ));
        break;
      case 9:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => SignInScreen(),
        ));
        break;
    }
  }
}
