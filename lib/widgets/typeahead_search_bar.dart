import 'package:druginfomobileapp/service/service_data.dart';
import 'package:druginfomobileapp/theme/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class TypeAheadSearchBar extends StatefulWidget {
  const TypeAheadSearchBar({Key? key}) : super(key: key);

  @override
  _TypeAheadSearchBarState createState() => _TypeAheadSearchBarState();
}

class _TypeAheadSearchBarState extends State<TypeAheadSearchBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Palette.textNo.withAlpha(10),
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: TypeAheadField(
              textFieldConfiguration: TextFieldConfiguration(
                textInputAction: TextInputAction.search,
                decoration: InputDecoration(
                  hintText: "Nhập tên thuốc bất kì...",
                  prefixIcon: Icon(Icons.search),
                  border: InputBorder.none,
                ),
              ),
              suggestionsCallback: (pattern) async {
                return await typeAhead.getTypeAhead(pattern);
              },
              itemBuilder: (context, Map<String, String> suggestion) {
                return ListTile(
                  title: Text(suggestion['title']!),
                );
              },
              onSuggestionSelected: (suggestion) {
                Navigator.pushNamed(context, "/drugsDetails");
              },
            ),
          ),
        ],
      ),
    );
  }
}
